# Start plumber server on ubuntu server

pm2 process manager

- start from sealr_useR2019 directory: 
```
pm2 start --watch --name meme-api --interpreter="Rscript" demo_rstatsmemes/backend/server.R
```
- stop: `pm2 stop meme-api`
- delete process: `pm2 delete meme-api`
